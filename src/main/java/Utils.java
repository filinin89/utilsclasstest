import java.math.BigInteger;

public class Utils {

    public String concatenateWords(String string1, String string2){
        String wholeWord = null;
        wholeWord = "";
        wholeWord = string1.concat(string2);
        return wholeWord;
    }



    public BigInteger computeFactorial(BigInteger n){ // нужен BigInteger, long слишком маленький для например 100!
        BigInteger result;
        if(n==BigInteger.valueOf(0) || n ==BigInteger.valueOf(1)) return BigInteger.valueOf(1);
        result = n.multiply(computeFactorial(n.subtract(BigInteger.valueOf(1))));
        return result;
    }









}
