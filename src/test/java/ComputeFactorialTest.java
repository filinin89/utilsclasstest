import org.junit.*;
import org.junit.rules.Timeout;

import java.math.BigInteger;
import java.util.Random;

import static junit.framework.Assert.assertEquals;
import static junit.framework.TestCase.assertNotNull;

public class ComputeFactorialTest {

    Utils utils;

    @Before
    public void initialize(){
        utils = new Utils();
    }

    @Test
    //@Ignore
    public void testComputeFactorial() {
        BigInteger checkedResult = BigInteger.valueOf(3628800); // вместо вызова другого метода сравним с подсчитанным внешним калькулятором результатом
        BigInteger result = utils.computeFactorial(BigInteger.TEN);
        assertEquals(checkedResult, result);
    }



    @Test(timeout = 10)
    public void testFactorialWithTimeout(){
        Random random = new Random();
        long number = Math.abs(random.nextInt(1000));
        BigInteger result = utils.computeFactorial(BigInteger.valueOf(number));
        System.out.println(result);
    }


}
