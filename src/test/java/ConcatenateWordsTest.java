import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;


public class ConcatenateWordsTest {

    private Utils utils;

    private static String string1 = "Tuc", string2="Tuc";
    private String result;

    @Before
    public void initialize(){
        utils = new Utils();
    }

    @Test
    public void testNotNullResult(){
        result = utils.concatenateWords(string1, string2);
        assertNotNull("Вы вернули null из метода", result); // он проходит тогда, когда результат null
    }

    @Test
    public void testNotEmptyResult() {
        result = utils.concatenateWords(string1, string2);
        assertNotEquals("Вы вернули пустой результат", "", result);
    }

    @Test
    public void testConcatenateWords(){

        result = utils.concatenateWords(string1, string2);
        assertEquals("Не верный результат", string1+string2, result);
    }

    @Test
    public void testNotLatinLetters() {
        result = utils.concatenateWords(string1, string2);
        Pattern pattern = Pattern.compile("^[a-zA-Z0-9]+$");
        Matcher matcher = pattern.matcher(result);
        assertTrue("Ваша строка состоит не из латинских букв", matcher.matches());
    }

    @AfterClass
    public static void close(){
        string1 = null; string2 = null;
    }









}
