import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        ComputeFactorialTest.class,
        ConcatenateWordsTest.class
})

public class Runner {
}
